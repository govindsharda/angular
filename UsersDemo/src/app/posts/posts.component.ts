import { Component } from '@angular/core';
import { PostsService } from '../Services/posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent {

  constructor(private svc:PostsService){}

   public posts:any;

  ngOnInit()
  {
   // this.svc.GetPosts().subscribe(res=>console.log(res));

   this.svc.GetPosts().subscribe((res:any)=>this.posts=res.data);
  }

}
