import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class PostsService {

 // apikey:any="asdhjfgjhs";
  //DI
  constructor(private httpsvc : HttpClient) { }
  //Method to fetch posts details from API
  GetPosts()
  {
    
   
  //  this.httpsvc.get(`http://www.omdbapi.com/?apikey=${apikey}&`)


    return this.httpsvc.get("https://dummyapi.io/data/v1/post",
    {
      headers:
      {
        "app-id":'6375219ceaea29aee2d19046'
      }
    });
  }

}
