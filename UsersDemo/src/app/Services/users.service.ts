import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  // Inject the dependancy
  constructor(private httpsvc:HttpClient) { }

  GetUsers()
  {
    //rturns a observable 
    return this.httpsvc.get("https://jsonplaceholder.typicode.com/users");
  }
}
