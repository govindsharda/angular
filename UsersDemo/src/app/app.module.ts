import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClient,HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { UsersService } from './Services/users.service';
import { UsersComponent } from './users/users.component';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './Services/posts.service';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    PostsComponent
  ],
  imports: [
    BrowserModule,HttpClientModule
  ],
  providers: [UsersService,PostsService, HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
