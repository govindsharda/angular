import { compileNgModule } from '@angular/compiler';
import { Component } from '@angular/core';
import { UsersService } from '../Services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent {

  public users:any;

  constructor(private svc:UsersService){}

  ngOnInit()
  {
    // obervable and Subsribe 
    
   // this.svc.GetUsers().subscribe(res=>console.log(res));
    this.svc.GetUsers().subscribe(data=>this.users=data);
  }
  



}
