import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FirstComponent } from './first/first.component';
import { HeaderComponent } from './header/header.component';
import { StructdrctveComponent } from './structdrctve/structdrctve.component';
import { AttrdirctveComponent } from './attrdirctve/attrdirctve.component';
import { CustomhighlightDirective } from './customhighlight.directive';
import { DatabindingdemoComponent } from './databindingdemo/databindingdemo.component';
import { PipedemoComponent } from './pipedemo/pipedemo.component';
import { CustompipePipe } from './custompipe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    HeaderComponent,
    StructdrctveComponent,
    AttrdirctveComponent,
    CustomhighlightDirective,
    DatabindingdemoComponent,
    PipedemoComponent,
    CustompipePipe
    //components, custom directives, custom pipes
  ],
  imports: [
    BrowserModule,
    FormsModule
    //Modules 
  ],
  providers: [], //services
  bootstrap: [AppComponent]
})
export class AppModule { }
