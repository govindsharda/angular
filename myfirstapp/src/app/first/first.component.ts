import { Component } from '@angular/core';

import {Book} from './book'

@Component({
  selector: 'my-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})


export class FirstComponent {
    
  //data => in the form of data memebers

  //business Logic within methods

  //if we are  writing some data manipulation logic
  // or fetching data from some APIs ...... within the method of a componenet
    // then it becomes not resuable
    
    // so we prefere making use of services

  books: Book[]=[
    new Book(),
    new Book()
  ];
  


}


