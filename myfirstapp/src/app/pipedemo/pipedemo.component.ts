import { Component } from '@angular/core';

@Component({
  selector: 'app-pipedemo',
  templateUrl: './pipedemo.component.html',
  styleUrls: ['./pipedemo.component.css']
})
export class PipedemoComponent {

    public today = new Date();
    public score=78.5678;
    public markperc=0.56;
    public salary=543678;

    public ussalary=435678;

    public myname ="ponnieaswari"

    public myjsondata ={
      Id: 100,
      Name:"ponni",
      Age:40
    }

}
