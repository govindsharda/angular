import { Directive, ElementRef ,Renderer2} from '@angular/core';

@Directive({
  selector: '[appCustomhighlight]'
})

export class CustomhighlightDirective {

  constructor(private elementref: ElementRef, private rend :Renderer2) { }

  ngOnInit() // one of the life cycle hook of angular, it is called once the component is created
  {
     this.rend.setStyle(this.elementref.nativeElement,'background-color','yellow');
  }

}
