import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StructdrctveComponent } from './structdrctve.component';

describe('StructdrctveComponent', () => {
  let component: StructdrctveComponent;
  let fixture: ComponentFixture<StructdrctveComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [StructdrctveComponent]
    });
    fixture = TestBed.createComponent(StructdrctveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
