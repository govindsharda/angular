import { Component } from '@angular/core';

@Component({
  selector: 'app-attrdirctve',
  templateUrl: './attrdirctve.component.html',
  styleUrls: ['./attrdirctve.component.css']
})
export class AttrdirctveComponent {
   isHighlighted : boolean = true;
   textcolor:string='green';
   fontsize:string ='15px';
   myname:string="Ponnieaswari";
}
