import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttrdirctveComponent } from './attrdirctve.component';

describe('AttrdirctveComponent', () => {
  let component: AttrdirctveComponent;
  let fixture: ComponentFixture<AttrdirctveComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AttrdirctveComponent]
    });
    fixture = TestBed.createComponent(AttrdirctveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
