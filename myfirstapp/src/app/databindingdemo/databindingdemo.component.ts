import { Component } from '@angular/core';

@Component({
  selector: 'app-databindingdemo',
  templateUrl: './databindingdemo.component.html',
  styleUrls: ['./databindingdemo.component.css']
})
export class DatabindingdemoComponent {
   public Name : string ="Ponnieaswari MS"
   public IsDisabled: boolean=false;
   public colorname:string="brown";
   public applystyle: boolean=true;
   public myNative :string="Kanyakumari"


   Display(desg:string)
   {
    alert(`Hi my designation is ${desg}`);
   }
}
