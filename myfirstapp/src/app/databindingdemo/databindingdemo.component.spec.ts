import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatabindingdemoComponent } from './databindingdemo.component';

describe('DatabindingdemoComponent', () => {
  let component: DatabindingdemoComponent;
  let fixture: ComponentFixture<DatabindingdemoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DatabindingdemoComponent]
    });
    fixture = TestBed.createComponent(DatabindingdemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
