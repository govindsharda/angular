import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';
import { HomeComponent } from './home/home.component';
import { Sub1Component } from './sub1/sub1.component';
import { Sub2Component } from './sub2/sub2.component';



const routes: Routes = [
  {path:"Home",component:HomeComponent,
  children:
  [
     {path:"sub1",component:Sub1Component},
     {path:"sub2",component:Sub2Component}
  ]},

  {path:"Aboutus",component:AboutusComponent},
  {path:"Contactus",component:ContactusComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
