console.log("Welcome to TypeScript")


// Staticaly typed language
var Age:number=22;
//Age="Welcome"; // Error: Type 'string' is not assignable to type 'number'
let score:number=89.99;

var Name:string="John";
var isUpdated:boolean=true;

// Array
var list1:number[]=[1,2,3];
var namelist:string[];
var tech:Array<string>=["dotnet","java","angular"];

//anonymous type
let Employee:[number,string,boolean,string]=[1,"John",true,"Admin"];

// Enum
enum Weekdays{Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday};

console.log(Weekdays[0]); // Sunday
console.log(Weekdays[6]); // Saturday

enum Weekdays1{Sunday=100,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday};
console.log(Weekdays1[100]); // Sunday
console.log(Weekdays1[101]); // Monday

console.log(Weekdays1["Sunday"]); // 100
console.log(Weekdays1.Thursday); // 104

// Any , when we are not sure about the type
let data:any;
data=10;
data="Welcome";
data=true;

function Add(first_number:number,second_number:number):number{
    return first_number+second_number;
}

Add(12,34);
//Add("Hello","World"); // Error: Argument of type 'string' is not assignable to parameter of type 'number'.

// Optional parameter
function Difference(first_number:number=100,second_number:number=0):number{
    return first_number-second_number;
}

Difference(12,34);
Difference(12);
Difference();


// function returning void
function print():void{
    console.log("Welcome to TypeScript");
}   


function Addnumandstrings(first_number:number,second_number:number):number;

function Addnumandstrings(first_number:string,second_number:string):string;

function Addnumandstrings(first_number:any,second_number:any):any{
    return first_number+second_number;
}

Addnumandstrings(12,34);
Addnumandstrings("Hello","World");
//Addnumandstrings(12,"World"); // Error: Argument of type 'string' is not assignable to parameter of type 'number'.


// OOPS Concepts
class Trainee
{
    // Fields
     Id:number; // default is public
    private Name:string; 
    public Email:string;

    // Constructor
    constructor(id:number,name:string,email:string){
        this.Id=id;
        this.Name=name;
        this.Email=email;
    }

    // Methods
    public Display():void{
        console.log("Id="+this.Id);
        console.log("Name="+this.Name);
        console.log("Email="+this.Email);
    }
}

let tobj:Trainee=new Trainee(1,"John","sam@jhgj");

tobj.Display();

// class with shortcut of fields and constructor
class Person{
    // No private data members are needed
    constructor(private id:number,private name:string,private email:string){
       // no need to write this.id=id;
    }
    public Display():void{
        console.log("Id="+this.id);
        console.log("Name="+this.name);
        console.log("Email="+this.email);
    }
}

let pobj:Person=new Person(1,"John","sam@jhgj");
pobj.Display();


// Inheritance
interface Animal
{
    name:string;
    sound:string;
    eat(food :string):void;
}

class Dog implements Animal
{
    name: string;
    sound:string;
    constructor(name:string)
    {
        this.name=name;
        this.sound="BowBow"
    }
  eat(food:string): void{
    console.log(`${this.name} is eating ${food}`);

  }

bark():void{
    console.log(`${this.name} says ${this.sound}`)
}
}


abstract class Shape
{
    abstract getArea():number;
}


class Circle extends Shape 
{

   constructor()
   {
    super()
   }
}

