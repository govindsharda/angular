console.log("Welcome to TypeScript");
// Staticaly typed language
var Age = 22;
//Age="Welcome"; // Error: Type 'string' is not assignable to type 'number'
var score = 89.99;
var Name = "John";
var isUpdated = true;
// Array
var list1 = [1, 2, 3];
var namelist;
var tech = ["dotnet", "java", "angular"];
//anonymous type
var Employee = [1, "John", true, "Admin"];
// Enum
var Weekdays;
(function (Weekdays) {
    Weekdays[Weekdays["Sunday"] = 0] = "Sunday";
    Weekdays[Weekdays["Monday"] = 1] = "Monday";
    Weekdays[Weekdays["Tuesday"] = 2] = "Tuesday";
    Weekdays[Weekdays["Wednesday"] = 3] = "Wednesday";
    Weekdays[Weekdays["Thursday"] = 4] = "Thursday";
    Weekdays[Weekdays["Friday"] = 5] = "Friday";
    Weekdays[Weekdays["Saturday"] = 6] = "Saturday";
})(Weekdays || (Weekdays = {}));
;
console.log(Weekdays[0]); // Sunday
console.log(Weekdays[6]); // Saturday
var Weekdays1;
(function (Weekdays1) {
    Weekdays1[Weekdays1["Sunday"] = 100] = "Sunday";
    Weekdays1[Weekdays1["Monday"] = 101] = "Monday";
    Weekdays1[Weekdays1["Tuesday"] = 102] = "Tuesday";
    Weekdays1[Weekdays1["Wednesday"] = 103] = "Wednesday";
    Weekdays1[Weekdays1["Thursday"] = 104] = "Thursday";
    Weekdays1[Weekdays1["Friday"] = 105] = "Friday";
    Weekdays1[Weekdays1["Saturday"] = 106] = "Saturday";
})(Weekdays1 || (Weekdays1 = {}));
;
console.log(Weekdays1[100]); // Sunday
console.log(Weekdays1[101]); // Monday
console.log(Weekdays1["Sunday"]); // 100
console.log(Weekdays1.Thursday); // 104
// Any , when we are not sure about the type
var data;
data = 10;
data = "Welcome";
data = true;
function Add(first_number, second_number) {
    return first_number + second_number;
}
Add(12, 34);
//Add("Hello","World"); // Error: Argument of type 'string' is not assignable to parameter of type 'number'.
// Optional parameter
function Difference(first_number, second_number) {
    if (first_number === void 0) { first_number = 100; }
    if (second_number === void 0) { second_number = 0; }
    return first_number - second_number;
}
Difference(12, 34);
Difference(12);
Difference();
