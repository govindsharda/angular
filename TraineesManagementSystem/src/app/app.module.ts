import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClient,HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms'

import { AppComponent } from './app.component';
import { TraineesService } from './Services/trainees.service';
import { TraineesComponent } from './trainees/trainees.component';

@NgModule({
  declarations: [
    AppComponent,
    TraineesComponent
  ],
  imports: [
    BrowserModule,HttpClientModule,FormsModule
  ],
  providers: [TraineesService,HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
