import { Component } from '@angular/core';
import { TraineesService } from '../Services/trainees.service';

import { Trainee } from '../Models/Trainee';

@Component({
  selector: 'app-trainees',
  templateUrl: './trainees.component.html',
  styleUrls: ['./trainees.component.css']
})
export class TraineesComponent {
  public traineeslist?:any
  constructor(private svc:TraineesService){}

  public trainee= new Trainee(); 
  public id?:any;
ngOnInit()
{
   this.GetTrainees();
}


  GetTrainees()
  {
    this.svc.GetTrainees().subscribe(data=>this.traineeslist=data);
  }
  
  AddTrainee()
  {
    this.svc.PostTrainee(this.trainee).subscribe();
    this.GetTrainees();
  }

  GetTraineeById(id: number)
  {
    this.id=id;
    this.svc.GetTraineebyId(id).subscribe((data:Trainee)=>this.trainee=data);
  }

  UpdateTrainee()
  {
    this.svc.UpdateTrainee(this.id,this.trainee).subscribe();
    this.GetTrainees();
  }

  DeleteTrainee(id:number)
  {
    this.svc.DeleteTrainee(id).subscribe();
    this.GetTrainees();
  }

}
