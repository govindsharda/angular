import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { environment } from 'src/environments/environment';
import { Trainee } from '../Models/Trainee';

@Injectable({
  providedIn: 'root'
})
export class TraineesService {

  //dependancy Injection
  constructor(private httpsvc :HttpClient){}

  GetTrainees()
  {
    return this.httpsvc.get(environment.apiURL);
  }

  PostTrainee(tboj:Trainee)
  {
    return this.httpsvc.post(environment.apiURL,tboj);
  }

  GetTraineebyId(id:number)
  {
    return this.httpsvc.get(`${environment.apiURL}/${id}`)
  }


  UpdateTrainee(id:any,tobj:Trainee)
  {
    return this.httpsvc.put(`${environment.apiURL}/${id}`,tobj)
  }

  DeleteTrainee(id:number)
  {
    return this.httpsvc.delete(`${environment.apiURL}/${id}`)
  }

 
}
