import { Component } from '@angular/core';
import { DataService } from '../MyServices/data.service';

@Component({
  selector: 'app-sampleservicedemo',
  templateUrl: './sampleservicedemo.component.html',
  styleUrls: ['./sampleservicedemo.component.css']
})
export class SampleservicedemoComponent  {


   public dispdate?:Date; 
   public names?:string[];

  constructor(private svc:DataService){}
  

  ngOnInit()
  {
  //  this.dispdate= this.svc.ShowTodaysDate();
this.names=  this.svc.ShowTodaysDate();
   }

}
