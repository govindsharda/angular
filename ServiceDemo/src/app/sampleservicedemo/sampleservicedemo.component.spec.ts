import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleservicedemoComponent } from './sampleservicedemo.component';

describe('SampleservicedemoComponent', () => {
  let component: SampleservicedemoComponent;
  let fixture: ComponentFixture<SampleservicedemoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SampleservicedemoComponent]
    });
    fixture = TestBed.createComponent(SampleservicedemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
