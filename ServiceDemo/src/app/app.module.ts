import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { DataService } from './MyServices/data.service';
import { SampleservicedemoComponent } from './sampleservicedemo/sampleservicedemo.component';

@NgModule({
  declarations: [
    AppComponent,
    SampleservicedemoComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
