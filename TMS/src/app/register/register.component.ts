import { Component } from '@angular/core';
import {FormBuilder,Validators} from '@angular/forms'
import { DataService } from '../services/data.service';
import {MatSnackBar} from '@angular/material/snack-bar'
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

constructor(private fb:FormBuilder,private svc:DataService,private _snackBar:MatSnackBar,private route:Router){}

public registerform= this.fb.group(
  {
    firstname:["",Validators.required],
    lastname:["",Validators.required],
    email:["",Validators.required],
    city:["",Validators.required],
    age:["",Validators.required],
    password:["",Validators.required]
  })
 

  get firstname(){return this.registerform.get("firstname")}
  get lastname(){return this.registerform.get("lastname")}
  get email(){return this.registerform.get("email")}
  get city(){return this.registerform.get("city")}
  get age(){return this.registerform.get("age")}
  get password(){return this.registerform.get("password")}


  openSnackBar(message: string) {
    this._snackBar.open(message,"Close",
    {
      duration:2000,
      verticalPosition:"top",
      horizontalPosition:"right"
    });
  }

  OnSubmit()
  {
    if(this.registerform.valid)
    {
     // console.log(this.registerform.value)
     this.svc.Register(this.registerform.value).subscribe((data:any)=>
      {
        this.openSnackBar(data.message);
        this.route.navigateByUrl("/Login")
          //console.log(data);
      },
      ((err:any)=>
      {
         this.openSnackBar(err.error.message);
      }
      ));
    }
  }


}
