import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms'
import { DataService } from '../services/data.service';
import { MatSnackBar } from '@angular/material/snack-bar'
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  public username = new FormControl("", Validators.required);
  public password = new FormControl("", Validators.required);

  constructor(private svc: DataService, private _snackBar: MatSnackBar,private route:Router) { }


  openSnackBar(message: string) {
    this._snackBar.open(message, "Close",
      {
        duration: 2000,
        verticalPosition: "top",
        horizontalPosition: "right"
      });
  }

  OnSubmit() {
    if (this.username.valid && this.password.valid) {
      // console.log(this.username.value, this.password.value);
      this.svc.Login({ email: this.username.value, password: this.password.value }).subscribe((data: any) => {
       // console.log(data);
       localStorage.setItem("token",data.access_token)
       this.route.navigateByUrl("/Home");
       
      }, (err: any) => {
        localStorage.removeItem("token");
        this.openSnackBar(err.error.message);
      }
      )
    }
    ;
  }
}

