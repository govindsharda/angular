import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddtraineeComponent } from './addtrainee/addtrainee.component';
import { AuthGuard } from './guards/auth.guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {path:"", component:HomeComponent},
  {path:"Home", component:HomeComponent},
  {path:"AddTrainee",component:AddtraineeComponent,canActivate:[AuthGuard]},
  {path:"Login",component:LoginComponent},
  {path:"Register", component:RegisterComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
