import { Component } from '@angular/core';
import {NgForm} from '@angular/forms'
import { Router } from '@angular/router';
import { Trainee } from '../Models/Trainee';
import { DataService } from '../services/data.service';
import {MatSnackBar, MatSnackBarModule} from '@angular/material/snack-bar';
@Component({
  selector: 'app-addtrainee',
  templateUrl: './addtrainee.component.html',
  styleUrls: ['./addtrainee.component.css']
})
export class AddtraineeComponent {

  constructor(private svc:DataService,private route:Router,private _snackBar: MatSnackBar){}

public traineeobj= new Trainee();

openSnackBar(message: string) {
  this._snackBar.open(message,"Close",
  {
    duration:2000,
    verticalPosition:"top",
    horizontalPosition:"right"
  });
}

  OnSubmit(frm:NgForm)
  {
    if(frm.valid)
    {
    //console.log(frm.value);
    this.svc.AddTrainee(frm.value).subscribe();
     this.openSnackBar("Trainee Created Successfully");
    this.route.navigateByUrl("/Home")
    
    }

  }
}
