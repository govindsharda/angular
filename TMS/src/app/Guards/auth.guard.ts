import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { throwError } from 'rxjs';
import {Router} from '@angular/router'
import { Observable,map,catchError } from 'rxjs';
import { DataService } from '../services/data.service';
@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

  constructor(private svc:DataService,private route:Router){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
  
    return this.svc.IsAuthenticated(localStorage.getItem("token")).pipe(map((data:any)=>
      {
         console.log(data);  
         if(data.isAuthenticated) {
           return true;
         }
         return false;
      }), 
      catchError((err:HttpErrorResponse)=>
      {
        this.route.navigateByUrl("/Login");
        return throwError(err);
        
      }))


    
  }
}







