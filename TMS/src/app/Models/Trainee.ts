export class Trainee
{
   public id?:number;
   public name?:string;
   public stream?:string;
   public score?:number;
}