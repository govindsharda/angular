import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Trainee } from '../Models/Trainee';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpsvc:HttpClient) { }

   AddTrainee(traineeobj :Trainee)
   {
    return this.httpsvc.post("http://localhost:3000/trainees",traineeobj)
   }

   GetTrainees()
   {
   return this.httpsvc.get("http://localhost:3000/trainees");
   }

   Register(user:any)
   {
     return this.httpsvc.post("http://localhost:9000/auth/register",user)
   }
   Login(user:any)
   {
    return this.httpsvc.post("http://localhost:9000/auth/login",user)
   }

   IsAuthenticated(token)
   {
    return this.httpsvc.post("http://localhost:9000/auth/isAuthenticated",null,
    {
      headers:{ "Authorization": `Bearer ${token}`}
    })
   }

}
